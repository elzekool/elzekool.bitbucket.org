class: center, middle, titlescreen

# Practical IoT for webdevelopers
## A talk by [@elzekool](https://github.com/elzekool)

???

Welcome to my talk "Practical IoT for webdevelopers"

---
# About me

* I work at Youwe as a Magento software architect
* I have a background in Electrical Engineering
* I have many hobbies, all related to technology, at home I have a lasercutter, cnc-mill and 3D printer that also grab my attention


---
# What is this talk about?

**First a disclaimer**: This talk showcases a lot of different technologies on different platforms and is partly dependent on cellular connectivity so let's all asume that big failures are going to happen ;-)

<br />

* **What is exactly "The Internet Of Things"?**<br >Try to keep awake during this part. Here I will tell what the Internet of Things is. What parts make up a solution and what technology is available.

* **Demo's**<br />The fun part (i hope). Here I showcase some devices I made to demonstrate a technique or a use-case.

---
# The definition

> The Internet of things (stylised Internet of Things or IoT) is the internetworking of physical devices, vehicles
> (also referred to as "connected devices" and "smart devices"), buildings, and other items—embedded with electronics,
> software, sensors, actuators, and network connectivity that enable these objects to collect and exchange data.
> https://en.wikipedia.org/wiki/Internet_of_things

<br />

* The definition of IoT is very broad; every device that somehow interacts with it's physical surroundings and has network connection is considered a IoT device.

* Network in this context is also very broad, networks are not only IP/ethernet networks but every method of connecting two or more devices.

* Is your phone "technicly" an IoT device? Yes! Is your computer an IoT device? Yes! Is this what most people think about with IoT? No

---
# The hype

.width-75[![Alt text](images/Hype-Cycle-for-the-Internet-of-Things-2016_Infographic.png)]<br />
[2 november 2016 / Gartner](http://www.gartner.com/smarterwithgartner/7-technologies-underpin-the-hype-cycle-for-the-internet-of-things-2016/)

---
# Useless use-cases

As IoT is so hyped a lot of critical voices can be heard that showcase useless or bad examples of IoT. I think it's good to highlight them
so that we can focus on the things IoT is usefull for:

* **Quirky Egg Minder Wink App Enabled Smart Egg Tray**<br />An Internet connected device that keeps track of the number of eggs still left in the device, when the egg is getting old and which egg is the oldest. Luckaly the device is only $12 on [Amazon](https://www.amazon.com/Quirky-Egg-Minder-Enabled-Smart/dp/B00GN92KQ4/)

* **Hydrate Spark - The smart water bottle**<br />A water bottle that connects to your phone or smartwatch and monitors how much water you drank so that you can "optimize your optimal water intake". Somehow made it past the Kickstarter phase and is available for €52 at their [site](http://hidratespark.com/)

* **Satis Smart Toilet**<br />A toilet that can be controlled over bluetooth, sprays, lifts the toilet seat and even monitors your "output". Usefull? No, Hacked? Yes!. Altrough I did not find any sites that sell them (it's a Japanese product) the retail price is around $4000

---
# Usefull applications

Usefull applications are most of the times not gamechanging devices but devices that do one small task that helps making an everyday task easier. Some examples:

* **Emergency call system for rapid assistance to motorists involved in a collision**. Newer cars have a system in place that automaticly calls the emergency service in case of an accident. From april 2018 this system is required in all new cars in Europe (eCall).

* **Realtime price cards** Like the ones seen in MediaMarkt allow realtime changes to prices based on what competitors do, what sales are doing, etc.

* **Smart trashcans** Municipalities spends lot's of money on garbage collection. For public trashcans a lot of money can be saved when the truck only comes by to empty them when needed. An example of this system is the "Big Belly" which not only indicates status but also compacts the trash.

---
# What makes up an IoT solution

* **Thing**: The device of sensor to make connected
* **Network**: The network that is used to connect the thing to other things or to the cloud
* ***Cloud***: A cloud service that interacts with things registrated to it <small>(optional but in 99% it is part of the solution)</small>.

---
# Network

One of the most influencal part of the solution is how to connect your thing to other things or to the cloud. A lot of initiatives are setup to improve
or provide this part.

Broad separation of types:

* **GSM/3G/4G (M2M):** average to high power usage, mobile, interaction-free setup, low data usage
* **Wifi**: high power usage, requires setup, location bound, high data usage
* **Bridged (Zigbee, Zwave, X10, Bluetooth)**: low to very low power usage, location bound, requires setup, low data usage
* **LPWAN (LoraWAN, LTE-MTC, SigFox)**: low to very low power usage, mobile, interaction-free setup, low data usage, early stage
* **Wired**: low to average power usage, location bound, interaction-free setup, high data usage

---
# Security

* Security is difficult in IoT! The main reasons for this are:<br /><br >
  * **Resource constraints**: Encryption and good random number generators can be memory and processor intensive. This makes that the number of posible encryption schemes is often limited.
  * **Limited access**: If connected trough an fast enough network updates can be rolled out over the air. Depending on the network used this is not always posible.
  * **Hardware/Software coupling**: Especially for consumer devices, updates to software will be supported for a limited time after new hardware is sold.
  * **Hardware tampering**: The device needs to be tamper proof even when someone has physical access.
  * **Valuable data**: Often the data transferred from and to a IoT device is valuable making it a target for mallicious entities.  


---
# Example 1 - "Lanceer Koffer" (1/2)

Made for my previous company VisualMedia the "Lanceer Koffer" is used to let the client itself launch their new or updated website. Where the company first added a temporary button to the website that the client could click on we took it to the next level with a suitcase that the client can use to launch their website.

.width-75[![Alt text](images/Nieuwe-website-Hoogeveen-07-08-2014.jpg)]<br />

---
# Example 1 - "Lanceer Koffer" (2/2)

**How it works:** On startup it goes trough all the Wifi networks it finds. It compares that list to a list on the SD card. If it finds a match it connects. On the SD card there are also sound files that indicate the different phases where the suitcase can be in.

On the button press it calls a webpage. This webpage does it's thing and returns "LAUNCHED", this triggers the happy music to play.

**Used technology**: Arduino, Wifi, SD card, MP3 decoder and amplifier. Two years ago the electronics where about €80,-. If rebuild today this would be around €25,- This is part due to the fact that IoT has got so much attention from manufacturers.

---
# Example 2 - "Fortune display" (1/2)

Demonstrating two key concepts, "Node-Red" and "MQTT" this demo shows random quotes and phrases on a screen that are wirelessly transmitted.

.width-75[![Alt text](images/mqtt-wireless-demo.jpg)]<br />

---
# Example 2 - "Fortune display" (2/2)

**How it works**: On the Raspberry PI runs a MQTT broker. Both the little device and Node-Red connect to this. Periodicly Node-Red fetches a "fortune" from a webserver and sends out an MQTT message. The display listens for this messages and when it's receives one it puts it on its display.

**Used technology**: [Node-Red](http://jolly-roger.local:1880/), [MQTT](http://www.hivemq.com/wp-content/uploads/Screen-Shot-2014-10-22-at-12.21.07.png), Wifi and an ESP8266. The ESP8266 is a chinese gamechanging device. It's about $1,50. The whole package (screen, battery, charging circuit and ESP8266 is about $14 where the battery is the most expensive part)

.width-100[![Alt text](images/mqtt-demo-overview.png)]<br />

---
# Example 3 - "The IoT store" (1/2)

Demonstrating the concept of AMQP, this demo prints out receipts for orders placed on a demo store. Where MQTT is realy made for the Internet of Things and can be challenging to interface with using more traditional web languages as PHP, AMQP is something that has good support.

.width-75[![Alt text](images/iot-demo-store.png)]<br />

---
# Example 3 - "The IoT store" (2/2)

**How it works**: On the Raspberry PI runs a MQTT broker. Both the little device and Node-Red connect to this. Periodicly Node-Red fetches a "fortune" from a webserver and sends out an MQTT message. The display listens for this messages and when it's receives one it puts it on its display.

**Used technology**: Raspberry Pi, [AMQP](https://www.rabbitmq.com/getstarted.html) and Wifi. AMQP is a powerfull protocol that supports different messaging use-cases (work queues, pub/sub, RPC, etc.). The support for this protocol is good troughout the different programming languages making it an ideal protocol for interconnecting them.

<br />
.width-100[![Alt text](images/iot-demo-overview.png)]<br />

---
# Example 4 - "GPS locator" (1/2)

Demonstrating a posible usage of 3G M2M communication this demo has a GPS device connected to a 3G enabled development board (Particle Electron). A website is made that requests the GPS data trough an API and displays the current status on a map and in a table.

<br />
.width-100[![Alt text](images/gps-example-screenshot.png)]<br />

---
# Example 4 - "GPS locator" (2/2)

**How it works**: Connected to the Particle Electron a GPS receiver is connected. The Electron reads the output of the GPS receiver and stores this in memory. The Particle API has functionality to share this variables with their cloud. The website uses the same cloud to fetch the variables and use them to update the map and table.

**Used technology**: Particle Electron, 3G, GPS and Particle Cloud. The Particle cloud is a easy to use cloud that handles a lot of things that are hard to do right (security, authentication and device provisioning).

---
# What to take with you

* The real power of IoT is not in customer applications. Smart devices can be usefull but will remain a niche compared to commercial applications.

* Think simple. The samples shown could be implemented without IoT hardware, a label printer with Wifi, an old smartphone, etc. could be used to implement most functionality.<br /><br /> .width-50[![Alt text](images/kiss.jpg)]

* Study message based protocols like AMQP. Not only is this very usefull for IoT applications, it is also a powerfull tool for normal webdevelopment.



---
# So you want to start with actual hardware?

####If you want to start with development on physical hardware:

.width-25-left[![Alt text](images/microbit-front.png)] A good board to start with is the Micro:Bit from the BBC. This board is distributed in the UK to a lot of school children to teach them programming. It is easy to start with (you can develop software in the browser) and has some sensors on board so you can create something real immediately.


####If you want to start with IoT development

.width-10-left[![Alt text](images/photon_vector2_600.png)] Particle is a company that makes development board but also created a cloud based solution to support it. This cloud makes it easy to work with your device. Programming can be done in a browser, firmware is uploaded over the air. The documentation is well written.



---
# Links

###Tools:
* **Node-Red**: https://nodered.org/
* **Particle**: https://www.particle.io/
* **Hypriot**: https://blog.hypriot.com/
* **Mosquitto**: https://mosquitto.org/
* **RabbitMQ**: https://www.rabbitmq.com/

###Examples:

* **Fortune display**: https://github.com/elzekool/Iot-MQTT-Example
* **IoT Store**: https://github.com/elzekool/IoT-Store-Example
* **GPS locator**: https://github.com/elzekool/IoT-GPS-Example
