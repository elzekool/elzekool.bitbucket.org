class: center, middle, titlescreen

# SOLR and Elasticsearch internals
## A talk by [@elzekool](https://github.com/elzekool)

???

Welcome to my talk about the internals of SOLR and Elasticsearch.

---
# About me

* I work at Youwe as a Magento software architect
* When I see a new technology my first response is not how to use it but how does it work
* I have many hobbies, all related to technology, at home I have a lasercutter, cnc-mill and 3D printer that also grab my attention

---

class: center, middle

# What this talk is not about

!["Not about"](images/not-about.svg)

Not about how to integrate Elasticsearch and SOLR into your application.

Not about which is better Elasticsearch or SOLR.

???
Maybe a strange thing to begin with, but I first want to explain where the talk
won't be about. I won't talk about how Elasticsearch or SOLR is integrated
into your PHP project, or how to install it or even how their API's look like.

I also won't tell you which is better then the other.
Personally I have a slight preference for SOLR as it is more bare-bones and
Elasticsearch does a little bit more of that vodoo-magic.

I think SOLR is harder to get started but when you want to use Elasticsearch
optimally this requires the same knowledge.

---

class: middle

# What this talk **is** about

It explains the parts that make up a search engine like SOLR and Elasticsearch,
which can be divided in 3 separate parts:

* Indexing<br />
  <small>The process of analyzing and storing data that should be searchable</small>
* Querying<br >
  <small>The process of finding data that matches a search question from the user</small>
* Ranking<br />
  <small>Ranking the data found by the query process so that more relevant data is presented first</small>

<br /><br />
.center[!["About"](images/about.svg)]

???
My talk is about the different parts that make up the search engine like SOLR
and Elasticsearch. With this information you can start using SOLR more easily and
start using Elasticsearch in a more advanced way.

The main engine consists of three parts, more precise: Indexing, Querying and Ranking.
These three things are related closely with each other.

Indexing is the process of analyzing and storing data that should be searchable.
This is an important process as search engines like SOLR and Elasticsearch don't
directly work from your application database but use their own storage.

Data needs to be put into the index. In most cases this is done trough application code
trough an XML or JSON API.

With Querying you are searching in the data was indexed.
This results that are found while querying the index can be ranked so that the
resultset first contain the most relevant results.

---

# Data

Data is indexed based on documents and fields. A document is analog to a table row and a field is analog to a table column. Like in a traditional SQL database system each field is configured to hold a specific type of content.

![Saving a document to the index](images/saving-a-document-to-the-index.svg)

???
When we talk about data it important to know to important terms: "Document" and "Field".
If you compare the index to a generic SQL database a document can be compared
to a database row. A document contains fields. This can be compared to a column
in a generic SQL database.

Just like a database column each field has a specific type. This type defines the
type of data that is stored and how it is handled.

Most of the work configuring the search engine is in those field type definitions.
By configuring them we can configure our search engine for the types of data
we need to index and query in our application.

Their lies the true strength of setting up our own search engine.
Generic search engines like "Google" or "Bing" work as good as the do because
they have access to enormous amounts of data, but using the same method for our
own data would not provide the same quality of results.

By knowing the type of data we need to search we can optimize our search engine for it.

---

# The relation between index and query
![The relation between index and query](images/relation-index-and-query.svg)

???
Before we can go into how the indexation process works it is important to know
how indexation and querying are related.

With indexing data in fields is put trough an analyzer so that it is converted
into terms.

The same process is done with the text that is in the query, it is put trough an
analyzer and is converted into terms.

After this the terms that are indexed and the terms that are extracted from the
search query are compared with each other making a match when they are the same.

---

# The relation between index and query (2)
![Sample index and query match](images/sample-index-query-match.svg)

???
Here you see a sample of how field data, in this case "The quick brown fox",
is put trough the analyzer and then become multiple terms.

The same process is done with the query "brown".

The term "brown" from the query and the term "brown" that was indexed matches so
we can say that the query matches the document that is associated with the term.

In this case the match is done on an exact term match, it is possible to do a
"fuzzy" match between terms, we handle that later, let's assume for now that we
only do exact matches.

It is important to know that the analyzer that is used during indexing can be
different for each field and also for indexing and querying.

In this example you also see that the word "quick" has become two terms,
"quick" and "fast". This is only as example, I will tell you later why this is
a bad idea for an index.

Terms also hold their position relative to each other. This is used one searching
for phrases.

---
class: center

# Analyzers

<br />

!["Analyzers"](images/analyzers.svg)

???
As shown an analyzer consists of three parts:

- CharFilter(s): The charfilters work on the whole query or field text. They do
  things like lowercasing the text, converting HTML to plain text or converting
  special characters to plain characters. As shown before matching terms is
  normally done on exact matches lowercasing the text while indexing and querying
  allows for case-insensitive search.

- Tokenizer: A tokenizer is used to split the query or field text into seperate
  terms or "tokens". How this is done is based on the tokenizer that is used.
  The default tokenizer uses whitespace and punctuation to split terms. It is
  also possible to use the whole query or field text as one single token.

- Filter(s): Filters work on the tokenized terms. In the example on the previous
  slide you saw an example of this with the synonym "fast" added for "quick".
  Filters are the most complex part of the analyzer and I will talk a little
  about some common filters.

---

# Filters: Synonym

The synonym filter allow the definition of synonyms, term(s) are added next to
the given term or they replace the given term.

quick: <small>fast, quick</small>

petrol: <small>gas, gasoline, petrol</small>

seperate: <small>separate</small>

elze: <small>eelke, elze</small>

eelke: <small>elze, eelke</small>

???

The name of the filter already tells it purpose. It allows you to define synonym(s).
It can replace a term with another or it can add new terms besides the term that is given.

It can also function as a method to change terms that are specific for a region in the
world, for example the British use "petrol" where the Americans use "gas" or "gasoline".

At last the synonym filter can be used to fix common spelling mistakes, like seperate
that should be written with an "a" instead of an "e".

If I where to make a search engine at our work I would add a synonym for me and "eelke",
our names are mixed up so often, you would think the do it on purpose. With this synonym
the at least find the correct pages.

Synonyms are something dynamic, it is commonly used for search refinements.
When you use them while indexing you need to reindex all documents when you changed
a synonym, if you only apply synonyms when querying you can change it on the fly.

---

# Filters: NGram

The NGram filter splits terms into smaller pieces and add them next to the given term.
A minimum and maximum size and edge can be given. Mostly used to allow the user to
search for partial words.

spaceshuttle (*min:3, max: 10, no edge*): <small>spa, pac, ace, ces, esh, shu, hut, utt, ttl, tle, spac, pace, aces, cesh, eshu, shut, hutt, uttl, ttle, space, paces, acesh, ceshu, eshut, shutt, huttl, uttle, spaces, pacesh, aceshu, ceshut, eshutt, shuttl, huttle, spacesh, paceshu, aceshut, ceshutt, eshuttl, shuttle, spaceshu, paceshut, aceshutt, ceshuttl, eshuttle, spaceshut, paceshutt, aceshuttl, ceshuttle, spaceshutt, paceshuttl, aceshuttle, spaceshuttl, paceshuttle, spaceshuttle</small>

spaceshuttle: (*min:3, max: 10, edge start*): <small>spa, spac, space, spaces, spacesh, spaceshu, spaceshut, spaceshutt, spaceshuttl, spaceshuttle</small>

spaceshuttle: (*min:3, max: 10, edge end*): <small>tle, ttle, uttle, huttle, shuttle, eshuttle, ceshuttle, aceshuttle, paceshuttle, spaceshuttle</small>



???

As told, matching of terms are based on exact matches. This may be a little
counter-intuitive for people that are used to "LIKE" searches. If a term is
indexed a partial query for this term won't match.

The solution for this is to also index partial terms. This can be done using the
NGram filter. You give a minimum and maximum size for the terms and you can tell
the filter only to start from the end and/or the beginning. In most cases this
configuration is a trade-off between speed (index size) and usability.

Where synonyms are mostly used in query analyzers NGram filters are mostly used
when indexing as using them at query time would most probably return results the
user would not expect.

[An Introduction to Ngrams in Elasticsearch](https://qbox.io/blog/an-introduction-to-ngrams-in-elasticsearch)

---

# Filters: Stemming

Stemming is converting words into their "base" form. Based on the exact implementation
chosen this is done on a algorithm or based on a dictionary/language rules.

jumping , jumps , jumped: <small>jump</small>

running , ran: <small>run</small>


???
Stemming is the procedure where a term is converted to it's base form. Examples are
"jumping" , "jumps" and "jumped" that all become "jump". There are multiple implementations
of stemmers but the can be divided in two categories.

The first one is algorithm based, it uses a limited set of rules to determine
the stem, in most cases algorithm based stemming will only remove suffixes. A popular
stemmer that uses this approach is the Snowball stemmer.

The other type is dictionary based, it uses a language dictionary to determine the
rules to stem terms. This method is far more accurate but also slower as it needs
to search the dictionary for each term. A popular stemmer that uses this approach is
the Hunspell stemmer that uses hunspell dictionaries to do it's stemming.

---

# Analyzers, some final notes

* There are a lot of CharFilters, Tokenizers and Filters. I recommend you go trough them
  before you define your analyzer. Also when you want to use an item do some research of the pro's and cons.

* It can be good to create multiple analyzers for the same field. Both SOLR and Elasticsearch
  have functionality for it, use a higher ranking factor for the more precise field.

* Both SOLR and Elasticsearch have tools to test your analyzer by putting in a text and
  outputting the terms that are generated, use it!

* Think about where you want to apply which analyzer, use it only for indexing or only for querying.

???
Some final tips on analyzers. First it is good to know that there are a lot more
CharFilters, Tokenizers and Filters then I described here. I recommend you to have a
look at the documentation of SOLR or Elasticsearch (depending on which you want to use)
and see what is available. If you want to use an item do some research of the pro's and cons.

Also one important thing to know is that a lot of times you want to use multiple analyzers
for the same field. Where you give the output of one analyzer a higher ranking factor
then the other. Both SOLR and Elasticsearch have functionality for this.

Also it's a good idea to check how your analyzers are performing. Both SOLR and Elasticsearch
have functionality on on board to check which terms are generated from a text.

Last thing, in many cases you want to use a different analyzer for indexing than
for querying. If there are already analyzers defined in your project test the analyzers
and make a discission which analyzer you want to use.

---

# The "Trie"

!["Example of a Trie"](images/example-of-a-trie.svg)

???
One of the reasons that SOLR and Elasticsearch can search so fast is that it
uses tries for retrieving data. The word comes from "retrieval". To distinguish the
word from a tree, most of the times it is pronounced as "try" or "try's".

Tries are a form of graph, that means that data is not only stored in the nodes but
also in the connection between nodes. At this slide you see a trie for 5 words. As you
can see the individual characters are stored on the connections between the nodes.
Each node contains at least one connection to a new node, this can be a middle node or an
end node. As you can see nodes share a common prefix, therefor this model is
sometimes called a prefix tree.

In this case the trie only contain characters, but the same can be applied to numbers or
other data, for example a number could be stored where each link contains number of digits.

---

class: center

# (Doubly) linked list

!["Doubly Linked List"](images/doubly-linked-list.svg)

???
To understand why the trie is such an effective way to search for information it
is useful to know how lists of data are normally stored. A common way of doing so
is trough a linked list. With such a list each element contains the data associated
with that element and when not the last element it also contains a pointer to
the next node. This way you can traverse from the beginning to the end of the list
by following the pointers.

When there is also a pointer back to the previous item, allowing the traversal in the
reverse direction, we call the list a doubly linked list.

There are a lot of ways to search this list but if we keep it simple you can see how
much faster it is to search for a term in the trie, you just have to follow each letter,
if it is not there you know it is not in the index. Worst case for the linked list is that
you have to go trough each element and compare it to the term searched, only knowing
at the end if the term is available.

---

# Fuzzy search

!["Levensthein Distance Matrix"](images/levensthein-distance-matrix.png)

Source: http://ntz-develop.blogspot.nl/2011/03/fuzzy-string-search.html

???

An even bigger advantage is when we want to do fuzzy search. This means we try to
find terms that look like the term we have given but don't have to be exactly the same.
One way of calculating how similar terms are is by calculating their levensthein distance.

Levensthein is a method where each change is given a value. A change can be an
edit (character replaced), a delete or an addition. The total of all operations
needed to transform one term into the other term is called the levensthein distance.

The normal way of calculating the levensthein distance between two terms is by using
a comparison matrix as can be seen in the slide. This matrix calculation is something that
can be optimized quite a lot, but unoptimized it must be calculated for each term that
is indexed, this is not very efficiënt.

With a Trie you can start at the root, when a replacement is needed perform it and
add the "cost" of the operation, when the cutoff distance is reached the rest of the
graph can be discarded. This is why tries are used very often in spelling correction
or term suggestions.

SOLR and Elasticsearch indexes don't store the whole dictionary in a trie as a trie requires
a lot of random access, so it uses a trie for the first part of the term so the trie can
be stored in memory, the remainder of the term is stored on disk, but when this is reached
a lot of terms where already discarded.

---

# Ranking

* **DisMax**:<br /> Similarity is calculated on a field level, DisMax is a very common way
to calculate a score for a whole document.

* **Tf-Idf**:<br /> Calculating the number of times a term occurs in a document and relate
this to the number of times the term occurs in all documents.

* **Cosine similarity**:<br /> Ranking documents relative to each other based on the terms
that the document and the query share.


???

After terms from the query are matched with terms from the index the next step is
to rank the results. This is done field by field, for each field a similarity score
to the query is calculated. More relevant documents have a higher score. The score
value is a relative value, this means that not the absolute value is important but
the relative scores between the documents.

Ranking, or calculating the score is very complicated, going trough all the methods
that make up this ranking system is impossible. I will focus on three important methods:
Tf-Idf, Cosine similarity and DisMax.

DisMax is the easiest one so I will handle that first. For every match between a document
and a field a score is calculated. DisMax is a very common way to calculate the document
score out of the field values. DisMax stands for Disjoint Maximum. It takes the maximum field score
for each document an takes that as the document score, so less scoring fields are not used.
For SOLR and Elasticsearch this is not entirely true as it is posible to use something called
a tie breaker that uses a percentage of the scores of other fields as an extra boost.

---

# Tf-Idf

**Tf = Term Frequency**

Formula: *Nr of times a term is a document* / *Number of terms in a document*

<br />

**Idf = Inverse Document Frequency**

Formula: 1 + log<sub>e</sub>(*Total nr of documents* / *Number of documents with term*)<br />

<br />

**Tf-Idf = Tf * Idf**

???

Tf-Idf is a method to determine how important a term is in the whole spectrum of terms.
It uses two numbers, TF which stands for Term Frequency and Idf which stands for
Inverse Document Frequency.

Tf causes the terms that are frequently found in a particular document to be regarded as
the most important.

Idf causes terms that are rarely found in documents to be regarded as more important.

Tf on its own would have a very weak performance because it would regard words that are
very common, like stopwords, as very important. But multiplied with Idf which uses a logarithmic
scale this very common words are not regarded as very important anymore, and only the words
that are common in a document and are rare will score high.

---

# Cosine similarity

Document 1: <small>the, game, of, life, is, a, game, of, everlasting, learning</small><br />
Document 2: <small>the, unexamined, life, is, not, worth, living</small><br />
Document 3: <small>never, stop, learning</small><br />
Query: <small>life, learning</small>

.width-50[!["Cosine similarity vector graph"](plot/cosine-similarity.png)]

<table cellspacing="0" cellpadding="8">
  <tr><th></th><th>Document 1</th><th>Document 2</th><th>Document 3</th><th>Query</th></tr>
  <tr><th>life</th><td>0.140550715</td><td>0.200786736</td><td>0</td><td>0.702753576</td></tr>
  <tr><th>learning</th><td>0.140550715</td><td>0</td><td>0.468502384</td><td>0.702753576</td></tr>
</table>
<small>Source: https://janav.wordpress.com/2013/10/27/tf-idf-and-cosine-similarity/</small>


???

Cosine similarity is a way to determine which items (in this case documents) are
the most similar. It does this by comparing vectors in an multi-dimensional plane.

Each direction in that plane stands for a particular fact. In our case this fact is
the Tf-Idf weight of the given term in a document.

As this beamer can only project a 2D plane we base our example on that. In a real system
the number of planes is determined by the terms that where matched in the different documents.

In the example there are 3 documents and a query. Each word in the document and query is
regarded as a seperate term. Below the document and query you can find a table with their
respective Tf-Idf weights. The weights and the given example is from a website "Seeking Wisdom",
the link for this is in the slide, this site contains all the math that is explained here,
it is realy a good resource.

When we plot the given values into a vector graph we see that the angle of Document 1 and
Query is the closest, after that Document 2 and after that Document 3. This way you can
clearly see the ranking between the documents, the smaller the angle, the more similar.

The cosine dot product of the vectors is what mathematically determines its similarity,
as this is not a very simple formula we leave it out, the resource mentioned before
gives a clear explanation of it.

Cosine similarity is a very powerfull method that can be used in other types of ranking as well,
for example matching interests between users.
