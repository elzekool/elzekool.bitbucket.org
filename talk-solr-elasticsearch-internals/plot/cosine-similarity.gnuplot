set xrange [0:1]
set xlabel 'life'
set yrange [0:1]
set ylabel 'learning'
plot 'query.dat' with vectors title 'Query' lw 4
replot 'document1.dat' with vectors title 'Document 1' lw 4
replot 'document2.dat' with vectors title 'Document 2' lw 4
replot 'document3.dat' with vectors title 'Document 3' lw 4
